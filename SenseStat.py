#!/usr/bin/python
# SenseStat v1.1
# -- Sense HAT statistics for Raspberry Pi 
from random import randint
from get_cpu_stats import GetCpuLoad							
import datetime
import time
import math
import md5
import os

# use Hardware Sense HAT or Sense HAT emulator (see: https://sense-emu.readthedocs.io/en/v1.0/install.html)
try:
	from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
	hardware_sense_hat=True
except:
	from sense_emu import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
	hardware_sense_hat=False


s = SenseHat()
green = (0, 255, 0)
greenlow = (0, 8, 0)
yellow = (255, 255, 0)
purple = (255, 0, 255)
blue = (0, 0, 255)
bluelow = (0, 0, 8)
red = (255, 0, 0)
white = (255,255,255)
whitelow = (8,8,8)
nothing = (0,0,0)
pink = (255,105, 180)

# set display rotation based on accelerometer info
def set_display_rotation():
  accel=s.get_accelerometer_raw()
  x = round(accel['x'], 0)
  y = round(accel['y'], 0)
  rot = 0
  if x == -1:
    rot=90
  elif y == -1:
    rot=180
  elif x == 1:
    rot=270
  s.set_rotation(rot)

def read_statfile(statfile,fields,pos=0):
	results={}
	try:
		stat=open(statfile,'r')
	except:
	      print('stat not found: ' + statfile); 
	for line in stat: 
		for field in fields:			
			if line.startswith(field):
				results[field]=float(line.split()[pos]);
	return results

def chregg():
	v=randint(100,200);c=[(v,v,v),(v,0,0),(0,v,0),(0,0,v)];t=[((0,4),(255,255,255) if randint(0,2) else (0,0,255)),((1,3),c[2]),((1,4),c[2]),((2,3),c[2]),((2,4),c[2]),((3,2),c[2]),((3,3),c[2]),((3,4),c[2]),((3,5),c[2]),((4,2),c[2]),((4,3),c[2]),((4,4),c[2]),((4,5),c[2]),((5,1),c[2]),((5,2),c[2]),((5,3),c[2]),((5,4),c[2]),((5,5),c[2]),((5,6),c[2]),((6,1),c[2]),((6,2),c[2]),((6,3),c[2]),((6,4),c[2]),((6,5),c[2]),((6,6),c[2]),((7,3),c[1]),((7,4),c[1])]; 	
	if int(datetime.datetime.now().strftime("%m%d%H"))<121507: return;
	for p in t: s.set_pixel(p[0][1],p[0][0],p[1]);

def get_netdev():
	netdev=None
	netdevs=['wlan0','enp2s0f1','eth0','enp5s0']
	for dev in netdevs:
		try:
			stat=open('/sys/class/net/' + dev + '/speed','r')
			netdev=dev
		except:
			print('failed to detect netdev: ' + dev)		
	print('Using netdev: ' + netdev)						
	return netdev

def get_netdev_speed(dev):	
	try:
		speed=open('/sys/class/net/' + dev + '/speed','r').readline()
	except:
		print('error reading network speed for: ' + netdev)
	return int(speed)

def netstats_start(dev):	
	try:
		rx=open('/sys/class/net/' + dev + '/statistics/rx_bytes','r').readline()
		tx=open('/sys/class/net/' + dev + '/statistics/tx_bytes','r').readline()
	except:
		print('error reading network stats for: ' + netdev)
	return {'rx': float(rx), 'tx': float(tx)}

def netstats_stop(dev,nstats_start,speed):	
	netstats={}
	nstats_stop=netstats_start(dev)
	for stat in nstats_start:
		netstats[stat]=(((nstats_stop[stat]-nstats_start[stat])*100)/speed)%100
	return netstats

def show_pixel_column(stats,colors,field,x):
	val = stats[field]		
	nrpixs=int(math.ceil(val/100*8))
	if nrpixs>8: nrpixs=8
	for pix in range(0,nrpixs): 				
		if field in colors:
			s.set_pixel(x,7-pix,colors[field])
		else:
			s.set_pixel(x,7-pix,green)


def show_pixel_percent(stats,fields,colors=[]):
	x=-1
	for field in fields:
		if type(field)==list:	
			x+=1			
			for entry in field:				
				show_pixel_column(stats,colors,entry,x)
		else:
			if field in stats:				
				x+=1
				show_pixel_column(stats,colors,field,x)
	
# make sure to change current dir to our script's path
os.chdir(os.path.dirname(os.path.realpath(__file__)))
cpu = GetCpuLoad()
s.clear()
display_stats=False
display_debug=False
cpustats=[]
memstats=[]
allstats=[]
#netdev='eth0'
#netdev='enp2s0f1'
netdev=get_netdev()
netdev_speed=get_netdev_speed(netdev)

# temperature sysfile to read
prevtemp = 0
temp_acc = True
temp_sysfile = '/sys/class/thermal/thermal_zone0/temp'
if os.path.isfile(temp_sysfile)==False: temp_sysfile = '/sys/class/hwmon/hwmon0/temp1_input'

while True:

	set_display_rotation()			
	netstart=netstats_start(netdev)
	cpustats=cpu.getcpuload()
	netstats=netstats_stop(netdev,netstart,netdev_speed)
	if display_debug: print(netdev_speed,netstats)
	if 'temp' in allstats: prevtemp=allstats['temp']
	
	#print(cpustats)
	memstats=read_statfile('/proc/meminfo',['MemTotal','MemFree','MemAvailable','SwapFree','SwapTotal'],1)
	#print(memstats)
	#netstats=read_statfile('/proc/net/netstat',['IpExt'],[7,8])
	#print(netstats)
	allstats=cpustats
	allstats['memuse']=(memstats['MemTotal']-memstats['MemFree'])*100/float(memstats['MemTotal'])
	allstats['memfree']=memstats['MemFree']*100/float(memstats['MemTotal'])
	allstats['swapuse']=(memstats['SwapTotal']-memstats['SwapFree'])*100/float(memstats['SwapTotal'])
	allstats['swapfree']=memstats['SwapFree']*100/float(memstats['SwapTotal'])
	allstats['tx']=netstats['tx']
	allstats['rx']=netstats['rx']
	netstats_order=['tx','rx']
	if netstats['rx']>netstats['tx']: netstats_order=['rx','tx']
		
	allstats['temp']=round(int(open(temp_sysfile).read())/1000)+10	
	if (temp_acc and prevtemp>allstats['temp']): allstats['temp']=prevtemp 
	#print(allstats['temp'] ,prevtemp)
	
	if display_debug: 
		print(allstats)

	for event in s.stick.get_events(): 
		if event.action == ACTION_PRESSED: display_stats=not display_stats				
		
	s.clear()
	if display_stats:		
		show_pixel_percent(allstats,['cpu0','cpu1','cpu2','cpu3','memuse','swapuse',netstats_order,'temp'],{'memuse':blue,'swapuse':bluelow,'memfree':pink,'swapfree':white,'tx':yellow,'rx':purple,'temp':red,})
	else: chregg()
